<?php
require 'gentab.php';
$rows = generate_data(4,3);
$header = array("C1", "C2", "C3");
echo generate_table($header, $rows);

?>

<style type="text/css">

  .TFtable1{
		width:100%; 
		border-collapse:collapse; 
	}

  .TFtable2{
		width:100%; 
		border-collapse:collapse; 
	}

  .TFtable3{
		width:100%; 
		border-collapse:collapse; 
	}

  .TFtable1 th{
		background: #90ee90;
	}

  .TFtable2 th{
		background: #90ee90;
	}
  
  .TFtable3 th{
		background: #90ee90;
	}

	.TFtable1 tr:nth-child(even) { 
		background:  #a7c1c4;
	}

  .TFtable3 tr:nth-child(odd) { 
		background: #a7c1c4;
	}
  
</style>